#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define SIGNAL_LENGTH 320

extern double InputSignal_f32_1kHz_15kHz[SIGNAL_LENGTH];

double calc_signal_mean(double *sig_src, size_t sign_len);
double calc_signal_variance(double *sig_src, size_t sig_len, double sig_mean);
double calc_signal_std(double sig_variance);


int main(void)
{
    double mean = calc_signal_mean(&InputSignal_f32_1kHz_15kHz[0], SIGNAL_LENGTH);
    printf("\n Mean =  %f\n", mean);
    double variance = calc_signal_variance(&InputSignal_f32_1kHz_15kHz[0], SIGNAL_LENGTH, mean);
    printf("\n Variance = %f\n", variance);
    double std = calc_signal_std(variance);
    printf("\n Standard Deviation = %f\n", std);

    return EXIT_SUCCESS;
}

double calc_signal_mean(double *sig_src, size_t sig_len)
{
    // sum up all values and divide through length
    double mean = 0.0;
    for (size_t i = 0; i < sig_len; ++i) {
        mean = mean + sig_src[i];
    }
    mean = mean / (double)sig_len;
    return mean;
}

double calc_signal_variance(double *sig_src, size_t sig_len, double sig_mean)
{
    double variance = 0.0;
    for (size_t i = 0; i < sig_len; ++i) {
        variance = variance + pow((sig_src[i] - sig_mean), 2);
    }
    variance = variance / (double)(sig_len - 1);
    return variance;
}

double calc_signal_std(double sig_variance)
{
    double std = sqrt(sig_variance);
    return std;
}
